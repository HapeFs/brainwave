<?php
    session_start();
    include_once '../includes/config.php';
    include_once '../includes/db_connect.php';
?>
<html>
    <body>
        <title>Stichting Brainwave</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
          $(document).ready(function() {
            $.getScript('admin.js');
          });
        </script>
        <link href='../static/style.css' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Poiret+One|Titillium+Web' rel='stylesheet' type='text/css'>
        <meta name="google-site-verification" content="JGunN3lkIiLhn_93lri2mDk6gDEox9V9uz01yhpnoDs" />
    </header>
    <body style='height:100%;'>
        <div class="brainbox"><span class="brainwave">Stichting Brainwave</span><br />Management van de bar op de Faculteit der Natuurwetenschappen, Wiskunde en Informatica.
        </div>
        <div id='activities' class='activities'>
        <table id='activiteitable'>
          <thead style='display: block;'>
            <tr>
              <th>Naam activiteit</th>
              <th>Datum activiteit</th>
              <th><div id="addactivity" class="hbspecial" onclick="addActivity()"><big>+</big></div></th> 
            </tr>
          </thead>
          <tbody style='display: block; max-height: 50%; overflow-y; scroll;'>
            <tr>
            <td>bandjes ding </td>
            </tr>
          </tbody>
        </table>
        </div>
        <p class="contact">Voor meer informatie, neem contact op met het bestuur: bestuur@stichtingbrainwave.nl</p>
    </body>
</html>
