function addActivity(){ 
    var cover = document.createElement("DIV");
    cover.id= 'cover';
    cover.style.display='block';

    var window= document.createElement("DIV");
    window.id='window';
    window.style.width ="40%";

    window.innerHTML='<span class="floatMark" onclick="var elem = this.parentNode.parentNode; elem.parentNode.removeChild(elem);" onTouch="  event.preventDefault(); var elem = this.parentNode.parentNode; elem.parentNode.removeChild(elem);"><big> X </big></span>';


    var activity = document.createElement("FORM");
    activity.setAttribute('method', 'post');
    activity.setAttribute('action', 'activiteit.php');

    var description = document.createElement("FIELDSET")
    var legend1 = document.createElement("LEGEND");
    legend1.innerHTML = "Activiteit informatie:";
    description.appendChild(legend1);
    
    var naam = document.createElement("DIV");
    naam.innerHTML = "Naam activiteit:";
    description.appendChild(naam);
    var activityname = document.createElement("INPUT");
    activityname.type = "text";
    activityname.name = "activityname";
    activityname.id = "activity";
    description.appendChild(activityname);

    var omschrijving = document.createElement("DIV");
    omschrijving.innerHTML = "Omschrijving activiteit:"
    description.appendChild(omschrijving);
    var activitydescription = document.createElement("INPUT");
    activitydescription.type = "text";
    activitydescription.name = "description";
    description.appendChild(activitydescription);

    var datum = document.createElement("DIV");
    datum.innerHTML = "Datum activiteit:";
    var activitydate = document.createElement("INPUT");
    activitydate.type = "date";
    activitydate.name = "activitydate";
    description.appendChild(datum);
    description.appendChild(activitydate);

    var deadline = document.createElement("DIV");
    deadline.innerHTML = "Deadline inschrijving";
    var activitydeadline = document.createElement("INPUT");
    activitydeadline.type = "date";
    activitydeadline.name = "deadline";
    description.appendChild(deadline);
    description.appendChild(activitydeadline);

    var price = document.createElement("DIV");
    price.innerHTML = 'Prijs activiteit';
    var activityprice = document.createElement("INPUT");
    activityprice.type = 'number';
    activityprice.name = 'price';
    activityprice.step = '0.05';
    description.appendChild(price);
    description.appendChild(activityprice);
    
    var actief =document.createElement("DIV");
    actief.innerHTML = "Actief";
    var activityactive = document.createElement("INPUT");
    activityactive.type = "checkbox";
    activityactive.name = "active";
    description.appendChild(actief);
    description.appendChild(activityactive);

    var personal = document.createElement("FIELDSET")
    var legend2 = document.createElement("LEGEND");
    legend2.innerHTML = "Persoonlijke vragen:";
    personal.appendChild(legend2);

    var entryname = document.createElement("INPUT");
    entryname.type = 'text';
    entryname.name = 'namefield';
    entryname.setAttribute('value','Naam');
    entryname.innerHTML = 'Naam';
    entryname.setAttribute('readonly','true');
    personal.appendChild(entryname);

    var typefield = makeTypefield();
    personal.appendChild(typefield);

    var entryemail = document.createElement("INPUT");
    entryemail.type = 'text';
    entryemail.name = 'emailfield';
    entryemail.setAttribute('value','Email');
    entryemail.innerHTML = 'Email';
    entryemail.setAttribute('readonly','true');
    personal.appendChild(entryemail);
    var submit = document.createElement("INPUT");
    
    submit.type = 'submit';
    submit.innerHTML = 'Maak activiteit';

    activity.appendChild(description);
    activity.appendChild(personal);
    activity.appendChild(submit);
    
    
    window.appendChild(activity);
    cover.appendChild(window);
    document.body.appendChild(cover);
}

function makeTypefield(){
    var type = document.createElement("SELECT");
    type.name = 'type';
    var types = ['text','radio','checkbox','date'];
    for (var i = 0; i < types.length; i++) {
        var option = document.createElement("OPTION");
        option.value = types[i];
        option.innerHTML = types[i];
        type.appendChild(option);
    }
    return type;
}

function popupMessage(content,width){
    window.innerHTML='<span class="floatMark" onclick="var elem = this.parentNode.parentNode; elem.parentNode.removeChild(elem);" onTouch="  event.preventDefault(); var elem = this.parentNode.parentNode; elem.parentNode.removeChild(elem);"><big> X </big></span>';
    window.innerHTML+=content;
    cover.appendChild(window);
    document.body.appendChild(cover);
}

function updateMessage(content){
    var window= document.getElementById('window');
    window.innerHTML='<span class="floatMark" onclick="var elem = this.parentNode.parentNode; elem.parentNode.removeChild(elem);" onTouch=" event.preventDefault(); var elem = this.parentNode.parentNode; elem.parentNode.removeChild(elem);"><big> X </big></span>';
    window.innerHTML+=content;
}

function closePopup(){
    var cover= document.getElementById('cover');
    cover.remove();
}
