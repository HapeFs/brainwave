<?php 
  session_start();
  $username = $password = $hashed_psw ='';
  $username = secure_input($_POST["uname"]);
  $password = secure_input($_POST["psw"]);
  $hashed_psw = password_hash($password,PASSWORD_DEFAULT); 
  login($username,$password,$mysqli);

  function secure_input($input) {
      $input = trim($input);
      $input = stripslashes($input);
      $input = htmlspecialchars($input);
      return $input;
  }
  function login($username,$password,$mysqli){
    if ($stmt = $mysqli->prepare("SELECT id, username, password,permission FROM users WHERE username = ? LIMIT 1")) {
      $stmt->bind_param('s', $username);
      $stmt->execute();
      $stmt->store_result();
      $stmt->bind_result($user_id,$username,$db_password,$perm);
      $stmt->fetch();
      if (password_verify($password,$db_password)) {
        $user_browser = $_SERVER['HTTP_USER_AGENT']; 
        $user_id = secure_input($user_id);
        $_SESSION['user_id'] = $user_id;
        $_SESSION['username'] = $username;
        $_SESSION['logged_in'] = true;
        $_SESSION['permission'] = $perm;
        header('location: ../admin/admin.php');
      }else {
          $_SESSION['message']= 'Verkeerde gebruikersnaam of wachtwoord'; 
          echo $_SESSION['message'];
          //header('location: error.php');
      }
    }
  }  
?>
