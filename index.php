<?php
    session_start();
    require_once "includes/config.php";
?>
<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
      if (isset($_POST['login'])){
        require 'includes/config.php';
        require 'includes/db_connect.php';
        require 'login/login.php';
      }
    }
?>
    <html style='height:100%;'>
    <header>
<body>
<div id="id01" class="modal">
  <form class="modal-content animate" method="post" action="#">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <label for="uname"><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="uname" required>

      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw" required>
        
      <button type="submit"name='login'>Login</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
    </div>
  </form>
</div>

<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
        <title>Stichting Brainwave</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
          $(document).ready(function() {
            $.getScript('login/login.js');
            $.getScript('js/sha512.js');
          });
        </script>
        <link href='static/style.css' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Poiret+One|Titillium+Web' rel='stylesheet' type='text/css'>
        <meta name="google-site-verification" content="JGunN3lkIiLhn_93lri2mDk6gDEox9V9uz01yhpnoDs" />
    </header>
    <body style='height:100%;'>
        <div class="brainbox"><span class="brainwave">Stichting Brainwave</span><br />Management van de bar op de Faculteit der Natuurwetenschappen, Wiskunde en Informatica.
        <div id="admin"></div>
        </div>
        <p class="contact">Voor meer informatie, neem contact op met het bestuur: bestuur@stichtingbrainwave.nl</p>
    </body>
</html>
